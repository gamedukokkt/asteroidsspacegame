﻿using UnityEngine;
using System.Collections;

public class AsteroidScript : MonoBehaviour {
    public Transform nextAsteroid;
    public int score = 200;
    public int childAsteroids = 2;
    public float launchSpeed = 500;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnCollisionEnter2D(Collision2D c)
    {
        if (c.gameObject.CompareTag("Projectile") == true)
        {
            if (nextAsteroid != null)
            {
                for (int i = 0; i < childAsteroids; i++)
                {
                    float step = 360 / childAsteroids;
                    float angle = step * i * Mathf.Deg2Rad;
                    Vector2 newPostion = new Vector2(Mathf.Cos(angle), Mathf.Sin(angle));

                    Transform spawnedAsteroid;

                    spawnedAsteroid = (Transform) Instantiate(nextAsteroid, transform.position + (Vector3)newPostion, transform.rotation);
                    spawnedAsteroid.rigidbody2D.AddForce(newPostion*launchSpeed);


                    //Instantiate(nextAsteroid, transform.position + transform.right * i, transform.rotation);
                }
            }
            else
            {
                // Molemmat rivit ajavat saman asian
                // Player.Score = Player.Score + score;
                Player.Score += score;
            }

            Destroy(gameObject);
            Destroy(c.gameObject);
        }
    }
}
