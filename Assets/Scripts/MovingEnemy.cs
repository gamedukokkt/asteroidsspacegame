﻿using UnityEngine;
using System.Collections;

public class MovingEnemy : MonoBehaviour {
    Transform player;
    Vector2 playerDirection;
	// Use this for initialization
	void Start () {
        player = GameObject.FindWithTag("Player").transform;
	}
	
	// Update is called once per frame
	void Update () {      
        playerDirection = player.position - transform.position;

        /*
        Debug.DrawLine(transform.position, transform.position + (Vector3)playerDirection);
        float playerAngle = Vector2.Angle(Vector2.up, playerDirection);
        transform.rotation = Quaternion.Euler(0, 0, playerAngle-180);
        Debug.Log(playerAngle-180);
        */

        transform.rigidbody2D.AddForce(playerDirection);
	}
    void OnCollisionEnter2D(Collision2D c)
    {
        // Tarkistetaan ollaanko osuttu pelaajaan
        if (c.gameObject.tag == "Player")
        {
            c.gameObject.GetComponent<Player>().TakeDamage(49);
            Destroy(gameObject);
        }

        // Tarkistetaan ollaanko osuttu ammukseen
        if (c.gameObject.tag == "Projectile")
        {
            Destroy(gameObject);
        }
    }
}
