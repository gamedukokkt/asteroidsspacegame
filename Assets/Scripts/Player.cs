﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Player : MonoBehaviour {
    static public int Score = 0;
    public Text scoreText;

    float hp = 100;

    void FixedUpdate()
    {
        scoreText.text = "Pisteet: " + Score;
    }

    public void TakeDamage(float amount)
    {
        hp = hp - amount;
        if (hp <= 0)
        {
            Die();
        }
    }

    public void Die()
    {
        Application.LoadLevel(0);
    }
}
