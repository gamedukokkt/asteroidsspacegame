﻿using UnityEngine;
using System.Collections;

public class WarpGateScript : MonoBehaviour {
    public float range = 50.0f;
	// Use this for initialization
	void Start () {
        Vector2 newPosition = new Vector2();

        float minRange = range / 2 - range;   // esim. 100/2 = 50 ja 50-100 = -50
        float maxRange = range / 2;         // esim. 100/2 = 50

        int random = Random.Range(0, 2);
        if (random == 0)
        {
            newPosition.x = range;
            newPosition.y = Random.Range(minRange, maxRange);
        }
        else if (random == 1)
        {
            newPosition.y = range;
            newPosition.x = Random.Range(minRange, maxRange);

        }

        transform.position = newPosition;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter2D(Collider2D c)
    {
        Application.LoadLevel(0);
    }
}
