﻿using UnityEngine;
using System.Collections;

public class ShootingScript : MonoBehaviour {
    public Transform projectile;
    public float rateOfFire = 2.0f;
    float lastShot = -1;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {        
        if (Input.GetKey(KeyCode.Space) == true)
        {
            /* Lasketaan ero scenen aloittamisesta kuluneen ajan 
             * ja viimeisen ampumisen ajan välillä */
            float elapsedTime = Time.time - lastShot;   
            /* Lasketaan viive jakamalla yksi sekunti "ammuksia per sekunti"-arvolla */
            float delay = 1 / rateOfFire;
            if (elapsedTime > delay)
            {
                Instantiate(projectile, transform.position, transform.rotation);
                lastShot = Time.time;
            }
        }
	}
}
