﻿using UnityEngine;
using System.Collections;

public class AsteroidSpawner : MonoBehaviour {
    public int smallAsteroids = 3;
    public int mediumAsteroids = 5;
    public int largeAsteroids = 3;

    public Transform smallAsteroid;
    public Transform mediumAsteroid;
    public Transform largeAsteroid;

    public float spawnRange = 100.0f;

	// Use this for initialization
	void Start () {
        for (int i = 0; i < smallAsteroids; i++)
        {
            Vector2 asteroidPosition = new Vector2();

            // esim. rangelle 100 minimiarvo on -50 ja maksimi 50
            float minRange = spawnRange/2-spawnRange;   // esim. 100/2 = 50 ja 50-100 = -50
            float maxRange = spawnRange/2;              // esim. 100/2 = 50

            asteroidPosition.x = Random.Range(minRange, maxRange);
            asteroidPosition.y = Random.Range(minRange, maxRange);

            Instantiate(smallAsteroid, asteroidPosition, transform.rotation);
        }

        for (int i = 0; i < mediumAsteroids; i++)
        {
            Vector2 asteroidPosition = new Vector2();

            // esim. rangelle 100 minimiarvo on -50 ja maksimi 50
            float minRange = spawnRange / 2 - spawnRange;   // esim. 100/2 = 50 ja 50-100 = -50
            float maxRange = spawnRange / 2;              // esim. 100/2 = 50

            asteroidPosition.x = Random.Range(minRange, maxRange);
            asteroidPosition.y = Random.Range(minRange, maxRange);

            Instantiate(mediumAsteroid, asteroidPosition, transform.rotation);
        }
        for (int i = 0; i < largeAsteroids; i++)
        {
            Vector2 asteroidPosition = new Vector2();

            // esim. rangelle 100 minimiarvo on -50 ja maksimi 50
            float minRange = spawnRange / 2 - spawnRange;   // esim. 100/2 = 50 ja 50-100 = -50
            float maxRange = spawnRange / 2;              // esim. 100/2 = 50

            asteroidPosition.x = Random.Range(minRange, maxRange);
            asteroidPosition.y = Random.Range(minRange, maxRange);

            Instantiate(largeAsteroid, asteroidPosition, transform.rotation);
        }
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
