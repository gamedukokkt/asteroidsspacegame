﻿using UnityEngine;
using System.Collections;

public class LaserProjectile : MonoBehaviour {
    public float speed = 5.0f;
    public float duration = 2.0f;
    float spawnTime;
	// Use this for initialization
	void Start () {
        rigidbody2D.velocity = transform.up * speed;
        spawnTime = Time.time;
	}
	
	// Update is called once per frame
	void Update () {
        float elapsedTime = Time.time - spawnTime;

        if (elapsedTime > duration)
        {
            Destroy(gameObject);
        }

	}
}
