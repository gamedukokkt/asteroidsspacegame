﻿using UnityEngine;
using System.Collections;

public class FollowPlayer : MonoBehaviour {
    Transform player;
    Vector3 cameraPos;
	// Use this for initialization
	void Start () {
        player = GameObject.FindWithTag("Player").transform;
	}
	
	// Update is called once per frame
	void Update () {
        cameraPos = player.position;
        cameraPos.z = -1;

        transform.position = cameraPos;

	}
}
