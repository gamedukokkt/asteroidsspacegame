﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour {
    //public Vector2 moveDir = new Vector2(0, 0);
    public float speed = 2.0f;
    public float turnSpeed = 2.0f;
    float moveSpeed;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        /* Päivitetään moveDir-vektori */
        //moveDir.x = Input.GetAxis("Horizontal");
        //moveDir.y = Input.GetAxis("Vertical");

        moveSpeed = Input.GetAxis("Vertical") * speed;

        /* Käännetään alus */
        if (Input.GetAxis("Horizontal") != 0)
        {
            transform.Rotate(Vector3.forward, -Input.GetAxis("Horizontal") * turnSpeed);
            rigidbody2D.angularVelocity = 0;
        }

        /*
         * Vaihtoehto 1
         * Translate-metodi
         */
        //transform.Translate(moveDir);

        /*
         * Vaihtoehto 2
         * velocity-muuttuja
         */
        //rigidbody2D.velocity = moveDir;

        /*
         * Vaihtoehto 3
         * AddForce()
         */
        rigidbody2D.AddForce(transform.up * moveSpeed);
	
	}
}
